package com.edutec.clase20210611;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNumero;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNumero = findViewById(R.id.txtEdad);
    }

    public void prueba(View view) {
        // Generamos un valor aleatorio comprendido entre 0 y 100000 y lo almacenamos en la variable num:
        //num=(int)(Math.random()*100001);
        if(view.getId() == R.id.btnTest) {
            String edad = txtNumero.getText().toString();
            int nEdad = Integer.parseInt(edad);
            //Toast.makeText(this, "Edad: " + nEdad, Toast.LENGTH_SHORT).show();
            if(nEdad >= 18) {
                Intent actividad = new Intent(this, ActivityRuco.class);
                startActivity(actividad);
            } else {
                Intent actividad = new Intent(this, ActivityMocoso.class);
                startActivity(actividad);
            }
        }
    }

    public void onClose(View view) {
        System.exit(0);
    }
}