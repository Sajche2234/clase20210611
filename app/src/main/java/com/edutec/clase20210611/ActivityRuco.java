package com.edutec.clase20210611;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityRuco extends AppCompatActivity {
    private TextView txtN1_2, txtN2_2;
    private EditText txtRespuesta2;
    int n1 = (int)(Math.random()*11);
    int n2 = (int)(Math.random()*11);
    int resultadoInterno = n1 * n2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ruco);
        txtN1_2 = findViewById(R.id.txtN1_2);
        txtN2_2 = findViewById(R.id.txtN2_2);
        txtRespuesta2 = findViewById(R.id.txtRespuesta2);
        txtN1_2.setText(""+n1);
        txtN2_2.setText(""+n2);
        //
    }
    private void calculandoRespuesta(){
        String numero = txtRespuesta2.getText().toString();
        int result = Integer.parseInt(numero);
        if(result == resultadoInterno){
            Toast.makeText(this, "Excelente, Eres todo un Adulto.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Aún eres un Chiriso Mocoso! ;).", Toast.LENGTH_SHORT).show();
        }
    }

    public void prueba1(View view) {
        calculandoRespuesta();
    }

    public void onClose2(View view) {
        finish();
    }
}