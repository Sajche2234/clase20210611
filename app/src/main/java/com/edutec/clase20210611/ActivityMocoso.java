package com.edutec.clase20210611;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityMocoso extends AppCompatActivity {
    private TextView txtN1_1, txtN2_1;
    private EditText txtRespuesta1;
    int n1 = (int)(Math.random()*101);
    int n2 = (int)(Math.random()*101);
    int resultadoInterno = n1 + n2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mocoso);
        txtN1_1 = findViewById(R.id.txtN1_1);
        txtN2_1 = findViewById(R.id.txtN2_1);
        txtRespuesta1 = findViewById(R.id.txtRespuesta1);
        txtN1_1.setText(""+n1);
        txtN2_1.setText(""+n2);
    }
    private void calculandoRespuesta(){
        String numero = txtRespuesta1.getText().toString();
        int result = Integer.parseInt(numero);
        if(result == resultadoInterno){
            Toast.makeText(this, "Es correcto. ya no erees tan bebe.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Aún eres un benesote.", Toast.LENGTH_SHORT).show();
        }
    }

    public void prueba2(View view) {
        calculandoRespuesta();
    }

    public void onClose1(View view) {
        finish();
    }
}